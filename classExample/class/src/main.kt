class main {
    init {
        var myDog=DogClass()
        myDog.sleep()
        myDog.roam()
        myDog.makeNoise()


        var mySnake=SnakeClass()
        mySnake.sleep()
        mySnake.roam()
        mySnake.makeNoise()
        println(mySnake.animalCount)


        var myChicken=ChickenClass()
        myChicken.sleep()
        myChicken.roam()
        myChicken.makeNoise()
        println(myChicken.animalCount)
        println(myChicken.birdCound)
    }

}

fun main(args: Array<String>) {
    var myMain=main()
}